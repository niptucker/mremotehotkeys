#SingleInstance force

F1::
  DetectHiddenWindows, on
  WindowName := "mRemoteNG - confCons.xml"
  IfWinExist, %WindowName%
  {
    If WinVisible(WindowName)
    {
      WinHide, %WindowName%
      WinActivate ahk_class Shell_TrayWnd
    }
    else
    {
      WinShow, %WindowName%
      WinActivate, %WindowName%
    }
  }
  DetectHiddenWindows, off
  return

WinVisible(WinTitle)
{
	WinGet, Style, Style, %WinTitle%
	Transform, Result, BitAnd, %Style%, 0x10000000 ; 0x10000000 is WS_VISIBLE.
	if Result <> 0 ;Window is Visible
		Return 1
	Else  ;Window is Hidden
		Return 0
}