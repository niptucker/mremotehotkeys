Toggle mremoteng window with F1 hotkey

Install:

1. Download Autohotkey (www.autohotkey.com)
2. Download mremoteng_toggle.ahk script.
3. Doubleclick mremoteng_toggle.ahk to install it.
4. Launch mRemoteNG
5. Try to press F1